const oddEven = (input) => {
    let hasil
    if(input % 2 === 1) {
        hasil = input +'is odd number'
    } else if(input % 2 === 0) {
        hasil = input + 'is Even number'
    }
    return hasil
};

const input = 5;

console.log(oddEven(input));

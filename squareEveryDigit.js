const squareEveryDigit = (number) => {
    number = number.toString()
    let temp = ''
    for (let i = 0; i < number.length; i++) {
    temp += parseInt(number[i]) ** 2
    }
    return temp
};

const input = "1234";

console.log(squareEveryDigit(input));

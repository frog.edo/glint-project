const countWord = (word , input) => {

    let inputKecil = input.toLowerCase()
    let hurufKecil = word.toLowerCase()
    let output = inputKecil.split(hurufKecil)
    
    return output.length -1
};

const wordToCount = "dog";
const input = "Dogdogcatdogsheep";

console.log(countWord(wordToCount, input));

const tempConverter = (input) => {
    if (!isNaN (input)) {
        res = "Error"
    } else if(input[input.length - 1] === 'C'){
        let num = input.substring(0, input.length -1)
        res = `${parseInt((num *9 / 5)+ 32)}F`
    } else if(input[input.length - 1] === 'F'){
        let num = input.substring(0, input.length - 1)
        res = `${parseInt((num - 32) * 5 / 9)}C`
    }

    return res
};

const input = "35C";

console.log(tempConverter(input));
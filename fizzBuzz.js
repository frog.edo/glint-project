const fizzBuzz = (input) => {
    let hasil = ""
    for (let i = 1; i <= input; i++){
        if(i % 3 === 0 && i % 5 === 0){
            hasil += 'FizzBuzz '
        }else if(i % 3 === 0){
            hasil += 'Fizz '
        }else if(i % 5 === 0){
            hasil += 'Buzz '
        }else{
            hasil += i + ' '
        }
    }    

    return hasil
};

const input = 16;

console.log(fizzBuzz(input));

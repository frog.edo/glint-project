const palindrome = (input) => {
    let reverseWord = ''
    let output 

    for (let i = input.length - 1; i >= 0; i--) {
        reverseWord += input[i]

    }
    if (input === reverseWord) {
        output = " true "
    } else {
        output = `false (${input} is not match with ${reverseWord})`
    }
    return output
};

const input = "hello";

console.log(palindrome(input));

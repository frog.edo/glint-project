const capitalToFront = (input) => {
    let hurufBesar = ""
    let hurufKecil = ""

    for(let i = 0; i < input.length; i++){
        if (input[i] === input[i].toUpperCase()) {
            hurufBesar += input[i]
        } else {
            hurufKecil += input[i]
        }
    }
    return hurufBesar + hurufKecil
};

const input = "hApPy";

console.log(capitalToFront(input));


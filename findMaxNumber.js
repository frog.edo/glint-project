const findMaxNumber = (input) => {
    let maxNumber = input[0]

    for (let i = 0; i < input.length; i++) {
        if (input[i] > maxNumber ) {
        maxNumber = input[i]
        }
    }
    return maxNumber;
};

const input = [9, -2, -4, 8, 1]

console.log(findMaxNumber(input));
